# Bitbucket Pipe Demo

This repository is used to demonstrate the [rollbar/rollbar-notify](https://bitbucket.org/rollbar/rollbar-notify/src/master/) Bitbucket Pipe.  

The pipeline in the project is configured to report a build to https://rollbar.com/rollbar/bitbucket-pipe-demo/deploys/